package com.devpaul.filepickerlibrary;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.ColorRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.devpaul.filepickerlibrary.enums.FileScopeType;
import com.devpaul.filepickerlibrary.enums.FileType;

/**
 * Created by Paul on 11/23/2015.
 */
public class FilePickerBuilder {

    private Activity mActivity = null;
    private Fragment mFragment = null;
    private FileScopeType mType;
    private int requestCode;
    private int color;
    private FileType mimeType;

    /**
     * Builder class to build a filepicker activity.
     * @param activity the calling activity.
     */
    public FilePickerBuilder(Activity activity) {
        this.color = R.color.blue;
        this.mType = FileScopeType.ALL;
        this.mimeType = FileType.NONE;
        this.requestCode = FilePicker.REQUEST_FILE;
        this.mActivity = activity;
    }

    /**
     * Builder class to build a filepicker activity.
     * @param fragment the calling fragment.
     */

    public FilePickerBuilder(Fragment fragment) {
        this.color = R.color.blue;
        this.mType = FileScopeType.ALL;
        this.mimeType = FileType.NONE;
        this.requestCode = FilePicker.REQUEST_FILE;
        this.mFragment = fragment;
    }


    /**
     * Set the scopetype of the file picker.
     * @param type scope type. Can be DIRECTORIES or ALL.
     * @return the current builder instance.
     */
    public FilePickerBuilder withScopeType(FileScopeType type) {
        this.mType = type;
        return this;
    }

    /**
     * Set the request code of this. You can request a path to a file or
     * a directory.
     * @param requestCode the request code can be FilePicker.DIRECTORY or FilePicker.FILE.
     * @return current instance of the builder.
     */
    public FilePickerBuilder withRequestCode(int requestCode) {
        this.requestCode = requestCode;
        return this;
    }

    /**
     * Set the header color with a resource integer.
     * @param color the color resource.
     * @return current instance of tbe builder.
     */
    public FilePickerBuilder withColor(@ColorRes int color) {
        this.color = color;
        return this;
    }

    /**
     * Set the file mime type. The will require the returned file type to match the
     * mime type.
     * @param type the mime type.
     * @return current instance of the builder.
     */
    public FilePickerBuilder withMimeType(FileType type) {
        this.mimeType = type;
        return this;
    }


    /**
     * Build the current intent.
     * @return a filepicker intent, null if context cannot be obtained.
     */
    @Nullable public Intent build() {
        Intent filePicker = null;

        if (mActivity != null) {
            filePicker = new Intent(mActivity, FilePicker.class);
        } else if (mFragment != null) {
            Context context = mFragment.getContext();
            if (context != null) {
                filePicker = new Intent(context, FilePicker.class);
            }
        }
        if (filePicker != null) {
            filePicker.putExtra(FilePicker.SCOPE_TYPE, mType);
            filePicker.putExtra(FilePicker.REQUEST_CODE, requestCode);
            filePicker.putExtra(FilePicker.INTENT_EXTRA_COLOR_ID, color);
            filePicker.putExtra(FilePicker.MIME_TYPE, mimeType);
        }
        return filePicker;
    }

    /**
     * Builds and starts the intent with startActivityForResult() uses the
     * request code you said as the request code for the activity result.
     */
    public void launch() {
        Intent intent = this.build();
        if (mActivity != null) {
            mActivity.startActivityForResult(intent, requestCode);
        } else if (mFragment != null) {
            mFragment.startActivityForResult(intent, requestCode);
        }
    }
}
