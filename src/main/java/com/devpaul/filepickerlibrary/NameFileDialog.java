/*
 * Copyright 2014 Paul Tsouchlos
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.devpaul.filepickerlibrary;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.widget.EditText;

/**
 * Created by Paul Tsouchlos
 */
public class NameFileDialog extends AppCompatDialogFragment {

    private NameFileDialogInterface listener;
    private EditText fileName;

    @Override
    public void onAttach(Activity activity) {
        try {
            listener = (NameFileDialogInterface) activity;
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        listener = null;
        super.onDetach();
    }

    public static NameFileDialog newInstance() {

        NameFileDialog nfd = new NameFileDialog();
        nfd.setCancelable(false);
        return nfd;

    }

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle("New File");
        fileName = new EditText(getActivity());
        builder.setView(fileName)
               .setPositiveButton(R.string.file_picker_ok,
                       new DialogInterface.OnClickListener() {

                           @Override
                           public void onClick(DialogInterface dialog, int which) {
                               if (listener != null) {
                                   listener.onReturnFileName(fileName.getText().toString());
                               }
                           }
                       })
                .setNegativeButton(R.string.file_picker_cancel,
                        new android.content.DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

        final AlertDialog dialog = builder.create();
        return dialog;
    }
}
